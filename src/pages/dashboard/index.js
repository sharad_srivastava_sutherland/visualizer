import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './dashboard.routes';
import DashboardController from './dashboard.controller';
import dataManagerService from './dashboard.dataManager';
import visualizer from '../../services/visualizer.service';
import apiService from '../../services/api.service';

export default angular.module('app.dashboard', [uirouter, visualizer, apiService,dataManagerService])
  .config(routing)
  .controller('DashboardController', DashboardController)
  .name;