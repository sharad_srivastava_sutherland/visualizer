import angular from 'angular';

class DataManagerService {
    constructor() {
    }
getBarchartData(resp){
    var result = {};
    var data_one_arr = ['data1'];
    var data_two_arr = ['data2'];
    for(var i in resp){
         data_one_arr.push(resp[i].data1);
         data_two_arr.push(resp[i].data2);
     }
     result.data1 = data_one_arr;
     result.data2 = data_two_arr;
     return result;
   }
}
export default angular.module('dashboard.dataManagerService', [])
.service('dataManagerService',DataManagerService).name;