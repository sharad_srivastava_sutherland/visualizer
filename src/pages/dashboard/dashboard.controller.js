export default class DashboardController{
    constructor(visualizer,apiService,dataManagerService){
        this.visualizer = visualizer;
        this.apiService = apiService;
        this.dataManagerService = dataManagerService;
        this.barChart()
        this.pieChart()
        this.lineChart()
        this.areaChart()
        this.gaugeChart()
        this.donutChart()

    }
//    barChart(){
//        const barChartData = [
//            ['data1', 30, 200, 100, 400, 150, 250],
//            ['data2', 130, 100, 140, 200, 150, 50]
//        ];
//        const barChartConfig  = {'bindto':'#barchart','data':{'colors':{data1: 'pink',data2: 'brown'
//        }}};
//        this.visualizer.getBarChart(barChartData,barChartConfig);
//    }
    
     barChart(){
         var _this = this;
         this.apiService.getApiResponse({},'/api/dashboard?',function(resp) {
             var data = _this.dataManagerService.getBarchartData(resp);
             const barChartData = [data["data1"],data["data2"]]
             const barChartConfig  = {'bindto':'#barchart','bar':{'width':{'ratio':0.8}},'size':{'width':400},'data':{'colors':{data1: '#ff9900',data2: '#0000ff'
             }}};
             _this.visualizer.getBarChart(barChartData,barChartConfig);
         })  
     }
    pieChart(){
        const pieChartData = [
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50]
        ];
        const pieChartConfig  = {'bindto':'#piechart','size':{'width':400},'data':{'colors':{data1: '#ff0000',data2: '#00ff00'
        }}};
        this.visualizer.getPieChart(pieChartData,pieChartConfig);
    }
    lineChart(){
        const lineChartData = [
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50]
        ];
        const lineChartConfig  = {'bindto':'#linechart','size':{'width':400},'data':{'colors':{data1: 'red',data2: 'blue'
        }}};
        this.visualizer.getLineChart(lineChartData,lineChartConfig);
    }
    areaChart(){
        const areaChartData = [
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 100, 140, 200, 150, 50]
        ];
        const areaChartConfig  = {'bindto':'#areachart','data':{'colors':{data1: 'orange',data2: 'purple'
        },'types':{data1:'area',data2:'area-spline'}}};
        this.visualizer.getAreaChart(areaChartData,areaChartConfig);
        
    }
    gaugeChart(){
        const gaugeChartData =  [
            ['data', 91.4]
        ];
        const gaugeChartConfig = {'bindto':'#gaugechart','data':{'colors':{'data':'#F97600'}}}
        this.visualizer.getGaugeChart(gaugeChartData,gaugeChartConfig);
            
    }
    donutChart(){
        const donutChartData = [
            ['data1', 30],
            ['data2', 120],
        ];
        const donutChartConfig = {'bindto':'#donutchart','data':{'colors':{data1:'#F97600',data2:'purple'}}}
        this.visualizer.getDonutChart(donutChartData,donutChartConfig);
    }
}
DashboardController.$inject = ['visualizer','apiService','dataManagerService']
