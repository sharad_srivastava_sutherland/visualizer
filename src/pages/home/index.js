import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './home.routes';
import HomeController from './home.controller';
import visualizer from '../../services/visualizer.service';
export default angular.module('app.home', [uirouter,visualizer])
  .config(routing)
  .controller('HomeController', HomeController)
  .name;