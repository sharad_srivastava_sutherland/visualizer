export default class HomeController {
    constructor(visualizer) {
      this.visualizer = visualizer;
      this.barWithLineChart();
    }
    barWithLineChart(){
      const barWithLineChartData =  [
            ['data1', 30, 20, 50, 40, 60, 50],
            ['data2', 200, 130, 90, 240, 130, 220],
            ['data3', 300, 200, 160, 400, 250, 250],
            ['data4', 200, 130, 90, 240, 130, 220],
            ['data5', 130, 120, 150, 140, 160, 150],
            ['data6', 90, 70, 20, 50, 60, 120],
        ]	
       const barWithLineChartConfig = {'bindto':'#barWithLine','size':{'width':400},'data':{'colors':{data1: 'orange',data2: 'purple'
        },'types':{data3:'spline',data4:'line',data6:'area'},"type":'bar',"groups": [['data1','data2']]}};
        
       this.visualizer.getBarWithLineChart(barWithLineChartData,barWithLineChartConfig);
    }
  }
HomeController.$inject = ['visualizer']