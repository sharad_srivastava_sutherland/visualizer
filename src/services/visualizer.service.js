import angular from 'angular';
var c3 = require('../../node_modules/c3/c3.js')
const chartCache = {}
class Visualizer {
    
    constructor() {
        
    }

getCache(){
    return chartCache;
}
    
    
    getC3Chart(chartData,chartConfig){
        const final_chart_data = {};
        const data = {};
        const bar = {};
        const size = {};
        if(chartConfig.size){
            size.height = chartConfig.size.height?chartConfig.size.height:240;
            size.width = chartConfig.size.width?chartConfig.size.width:400;
        }
        if(chartConfig.data){
            data.columns = chartData;
            data.type = chartConfig.data.type?chartConfig.data.type:'';
            data.types = chartConfig.data.types?chartConfig.data.types:{};
            data.colors = chartConfig.data.colors?chartConfig.data.colors:{
                data1: '#ff0000',
                data2: '#00ff00'
            }
        }
        if(chartConfig.bar){
        bar.width = chartConfig.bar.width?chartConfig.bar.width:{ratio:0.5};
        }
        final_chart_data.size = size;
        final_chart_data.data = data;
        final_chart_data.bar = bar; 
        final_chart_data.bindto = chartConfig.bindto;
        var chart
        setTimeout(function(){
        chart = c3.generate(final_chart_data);
        },100)
        return chart
    }
    getLineChart(chartData,chartConfig) {
        
        chartCache[chartConfig.bindto] = {'type':'Line','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getPieChart(chartData,chartConfig){
        chartConfig.data.type = 'pie'; 
        chartCache[chartConfig.bindto] = {'type':'Pie','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getBarChart(chartData,chartConfig){
        chartConfig.data.type = 'bar';  
        chartCache[chartConfig.bindto] = {'type':'Bar','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getAreaChart(chartData,chartConfig){
        chartCache[chartConfig.bindto] = {'type':'Area','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getGaugeChart(chartData,chartConfig){
        chartConfig.data.type = 'gauge';
        chartCache[chartConfig.bindto] = {'type':'Gauge','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getDonutChart(chartData,chartConfig){
        chartConfig.data.type = 'donut';
        chartCache[chartConfig.bindto] = {'type':'Donut','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }
    getBarWithLineChart(chartData,chartConfig){
        chartCache[chartConfig.bindto] = {'type':'BarWithLine','data':chartData,'chartConfig':chartConfig}
        this.getC3Chart(chartData,chartConfig);
    }   
 }
export default angular.module('services.visualizer', [])
.service('visualizer',Visualizer).name;