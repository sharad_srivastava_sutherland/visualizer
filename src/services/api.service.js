import angular from 'angular';
class ApiService{
    constructor($http){
        this.$http = $http; 
    }
    getApiResponse(params,url,callback){
        var prefix = "http://127.0.0.1:5000";
        var request_params = "";
        for (const i in params) {
            if (params[i]) {
                request_params = request_params + i + "=" + encodeURIComponent(params[i]) + '&';
            }
        }
        request_params = request_params + "callback=JSON_CALLBACK";
        const req_url = prefix + url + request_params;
        this.$http.jsonp(req_url).success(function (data) { 
            callback(data);
        })
    }
}

export default angular.module('services.api-service', [])
.service('apiService',ApiService).name;
ApiService.$inject = ['$http'];
