import angular from 'angular';
import './left-nav.css';
export default angular.module('app.leftnav', [])
.component('leftnav',{
    template: require('./left-nav.html'),
    controller: function($scope,$location){
        $scope.showOpen = false;
        $scope.showClose = true;
        $scope.getClass = function (path) {
            return ($location.path().substr(0, path.length) === path) ? 'activelink' : '';
        }
        $scope.menuToggle = function(e,option) {
            e.preventDefault();
            console.log("option",option);
            var element = document.getElementById("wrapper");
            element.classList.toggle("toggled");
            if(option == 'open')
            {
                document.getElementById("sidebar-wrapper").style.width = "250px"
                $scope.showOpen = false;
                $scope.showClose = true;
            }
            else{
                document.getElementById("sidebar-wrapper").style.width = "70px"
                $scope.showOpen = true;
                $scope.showClose = false;
            }

        }
        
    }
        
}).name; 