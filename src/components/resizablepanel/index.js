import angular from 'angular';
import $ from 'jquery';
import './resizable-panel.css';
import visualizer from '../../services/visualizer.service'
export default angular.module('app.resizablepanel', [visualizer])
.component('panel',{
    bindings: {
        title: '@',
        chartId: '@',
        panelId: '@'    
    },
    template: require('./resizable-panel.html'),
    controller: function($scope,visualizer){
       var $ctrl = this;
       //this.$inject = ['visualizer']
       var callChartService = function(chartCache){
        if(chartCache['#'+$ctrl.chartId].type == 'Line'){
            visualizer.getLineChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'Bar'){
            visualizer.getBarChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'Pie'){
            visualizer.getPieChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'Area'){
            visualizer.getAreaChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'Gauge'){
            visualizer.getGaugeChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'Donut'){
            visualizer.getDonutChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
        if(chartCache['#'+$ctrl.chartId].type == 'BarWithLine'){
            visualizer.getBarWithLineChart(chartCache['#'+$ctrl.chartId].data,chartCache['#'+$ctrl.chartId].chartConfig);
        }
       }    
    
       $scope.chartWidth = 'width: 400px'
       $scope.maximizeChart = function (e,panel_id){
       var chartCache = visualizer.getCache();
       panel_id = '#'+panel_id; 
      
       if ($(panel_id).children('i').hasClass('glyphicon-resize-full'))
       {
        if(chartCache['#'+$ctrl.chartId].chartConfig.size){  
        chartCache['#'+$ctrl.chartId].chartConfig.size.width = 1200;
        }
        else{
        chartCache['#'+$ctrl.chartId].chartConfig.size = {};
        chartCache['#'+$ctrl.chartId].chartConfig.size.width = 1200;  
        }
        callChartService(chartCache);
        $(panel_id).children('i').removeClass('glyphicon-resize-full');
        $(panel_id).children('i').addClass('glyphicon-minus');
       }
        else if ($(panel_id).children('i').hasClass('glyphicon-minus'))
        {   if(chartCache['#'+$ctrl.chartId].chartConfig.size){ 
            chartCache['#'+$ctrl.chartId].chartConfig.size.width = 400;
            }
            else{
                chartCache['#'+$ctrl.chartId].chartConfig.size = {};
                chartCache['#'+$ctrl.chartId].chartConfig.size.width = 400;    
            }
            callChartService(chartCache);
            $(panel_id).children('i').removeClass('glyphicon-minus');
            $(panel_id).children('i').addClass('glyphicon-resize-full');
        }
   $(panel_id).closest('.panel').toggleClass("panel-fullscreen")
}
}
}).name