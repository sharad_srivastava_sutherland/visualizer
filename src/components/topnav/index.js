import angular from 'angular';
import './top-nav.css';
export default angular.module('app.topnav', [])
.component('topnav',{
    template: require('./top-nav.html')
}).name; 