import 'bootstrap/dist/css/bootstrap.css';
import 'c3/c3.css';
import 'jquery';
import 'bootstrap/dist/js/bootstrap.js';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import uibootstrap from 'angular-ui-bootstrap';
import routing from './app.config';
import home from './pages/home';
import dashboard from './pages/dashboard';
import leftnav from './components/leftnav';
import topnav from './components/topnav';
import resizablepanel from './components/resizablepanel';

angular.module('app', [uirouter, uibootstrap, home, dashboard, leftnav, topnav, resizablepanel])
  .config(routing);